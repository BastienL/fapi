<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static orderBy(string $string)
 * @method static where(string $string, false|string $uid)
 */
class Token extends Model
{
    use HasFactory;

    public static function create(array $all) {
        try {
            $model = new Token();
            $model->uid = md5(uniqid());
            $model->cours_id = Cours::where('uid', $all['cours_id'])->first()->id;
            $model->save();

            return $model;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function cours() {
        return $this->hasOne(\App\Models\Cours::class, 'id', 'cours_id');
    }
}
