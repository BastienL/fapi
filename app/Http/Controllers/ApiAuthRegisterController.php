<?php

namespace App\Http\Controllers;

use App\Models\User;
use http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ApiAuthRegisterController extends Controller
{
    private $client;

    /**
     * LoginController constructor.
     *
     * Set up the client to get secret info in private
     */
    public function __construct(){
        $this->client = \Laravel\Passport\Client::find(1);
    }

    /**
     * The user registration from the api.
     *
     * @return access_token and refresh_token
     */
    public function register(Request $request) {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'type' => 'required',
                'password' => 'required|min:6|confirmed'
            ]);
        } catch (ValidationException $e) {

        }

        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'type' => request('type'),
            'password' => bcrypt(request('password'))
        ]);

        $params = [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => request('email'),
            'password' => request('password'),
            'scope' => '*'
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }

}
